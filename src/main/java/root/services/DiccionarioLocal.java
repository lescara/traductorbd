/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import root.dto.Palabra;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("diccionario")

public class DiccionarioLocal {

    private String Palabra;
    private Palabra pal;
    
   @GET
   @Path("/{idbuscar}")
   @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(@HeaderParam("api-key") String apikey,@HeaderParam("api-id") String apiId, @PathParam("idbuscar") String idbuscar){
       
        System.out.println("REST diccionario:  apikey"+apikey);
        System.out.println("REST diccionario:  apiId"+apiId);
        System.out.println("REST diccionario :    palabra  "+idbuscar);
            
        Client client = ClientBuilder.newClient();
    
        WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2" + Palabra);
        pal = myResource.request(MediaType.APPLICATION_JSON).header("api-key","3a9dc6ba6b187235110af92790a6d5f3").header("api-id","6deff21c").get( root.dto.Palabra.class);
                 
        Palabra pal=new Palabra();
        pal.setPalabra(idbuscar);
        pal.setDescripcion("la palabra significa esto");
    
    
        return Response.ok(200).entity(pal).build();
    }
  

}
